Alex Salois description of  https://github.com/ryujaehun/pytorch-gpu-benchmark/blob/main/benchmark_models.py

Looking at the pytorch code.
It loads in loads in 8 different AI models.
Then creates random data sets.
Then trains and benchmarks the models.

It benchmarks the models using different precsions or bit depths.
Then it reports the times it took to complete the tasks. 
