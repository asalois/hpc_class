#!/bin/bash 
jb_id00=$((sbatch prime_array.sbatch) | cut -d " " -f 4 )
echo $jb_id00 && echo "prime_array.sbatch" 
jb_id06=$((sbatch --dependency=afterany:$jb_id00 summarize.sbatch) | cut -d " " -f 4 )
echo $jb_id06 && echo "summarize.sbatch" 
