#include <mpi.h>  
#include <stdio.h>  
#include <stdlib.h>  
#include <math.h>  

#define MASTER 0

int findPrime(int check){
int i,flag=0;
	for(int i=2; i<check/2; i++){
		if(check%i == 0){
		    flag=1;    
		    break;
		}    
	}
	if(flag==0){
		return check;
	}else{
		return 0;
	}
}


int main(int argc, char *argv[]){    
	int i,j,rc,start,range,numTasks,taskId,primeTx, primeRx;    
	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numTasks);
	MPI_Comm_rank(MPI_COMM_WORLD, &taskId);
	range = ceil(1000000.0/(float)numTasks);
	start = 1000000;
	for(j=start;j<(start+range);j++){
		if(taskId != MASTER){
			primeTx = findPrime(start+(j-start)*(numTasks-1)+taskId);
			rc = MPI_Send(&primeTx, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD);
		}else{
			for(i=1; i<numTasks; i++){
				rc = MPI_Recv(&primeRx, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
				if(primeRx != 0){printf("%d\n",primeRx);}
			}	
		}

	}     
	MPI_Finalize();
	return(0);  
}    
