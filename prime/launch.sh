#!/bin/bash 
jb_id00=$((sbatch prime_array_00.sbatch) | cut -d " " -f 4 )
echo $jb_id00 && echo "prime_array_00.sbatch" 
jb_id01=$((sbatch --dependency=afterany:$jb_id00 prime_array_01.sbatch) | cut -d " " -f 4 )
echo $jb_id01 && echo "prime_array_01.sbatch" 
jb_id02=$((sbatch --dependency=afterany:$jb_id01 prime_array_02.sbatch) | cut -d " " -f 4 )
echo $jb_id02 && echo "prime_array_02.sbatch" 
jb_id03=$((sbatch --dependency=afterany:$jb_id02 prime_array_03.sbatch) | cut -d " " -f 4 )
echo $jb_id03 && echo "prime_array_03.sbatch" 
jb_id04=$((sbatch --dependency=afterany:$jb_id03 prime_array_04.sbatch) | cut -d " " -f 4 )
echo $jb_id04 && echo "prime_array_04.sbatch" 
jb_id05=$((sbatch --dependency=afterany:$jb_id04 prime_array_05.sbatch) | cut -d " " -f 4 )
echo $jb_id05 && echo "prime_array_05.sbatch" 
jb_id06=$((sbatch --dependency=afterany:$jb_id05 summarize.sbatch) | cut -d " " -f 4 )
echo $jb_id06 && echo "summarize.sbatch" 
