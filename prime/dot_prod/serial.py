import time
from multiprocessing import Pool

def dot_prod(vec1,vec2):
	sum = 0
	for i in range(len(vec1)):
		sum += vec1[i] * vec2[i]
	return sum
#sequential
t = time .time()
num = 10000
v1= list(range(num))
v2= list(range(num))
ans = dot_prod(v1,v2)
print(len(v1))
print(len(v2))
print(ans)
t1 = time.time() - t
print(t1)
