import time
from multiprocessing import Pool

#parallel
results = []
def log_result(result):
	results.append(result)

def dot_prod(vec1,vec2):
	sum = 0
	with Pool(40) as p:
		for i in range(len(vec1)):
			p.apply_async(single_sum, args=(vec1[i],vec2[i],), callback = log_result)
		p.close()
		p.join()
	for i in range(len(vec1)):
		sum += results[i]
	return sum
	

def single_sum(j,k):
	return j*k

t = time .time()
num = 10000
v1= list(range(num))
v2= list(range(num))
ans = dot_prod(v1,v2)
print(len(v1))
print(len(v2))
print(ans)
t1 = time.time() - t
print(t1)
